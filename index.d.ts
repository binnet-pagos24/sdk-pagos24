import { Config } from './Providers/Config';
import { IConfig } from './interfaces/IConfig';
import { IUserApp } from './interfaces/IUserApp';
import { Payment } from './Providers/Payment';
import { Pos } from './Providers/Pos';
import { Country } from './Providers/Country';
import { Sector } from './Providers/Sector';
import { Card } from './Providers/Card';
export default class Pagos24 extends Config {
    userApp: IUserApp | undefined;
    payment: Payment;
    card: Card;
    country: Country;
    pos: Pos;
    sector: Sector;
    constructor(config: IConfig);
    /**
     * @description Inizialite and get data user
     * @return Promise<void>
     */
    private init;
}
