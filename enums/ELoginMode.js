"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ELoginMode = void 0;
var ELoginMode;
(function (ELoginMode) {
    ELoginMode["Window"] = "window";
    ELoginMode["Redirect"] = "redirect";
})(ELoginMode = exports.ELoginMode || (exports.ELoginMode = {}));
