"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ECardholderType = void 0;
var ECardholderType;
(function (ECardholderType) {
    ECardholderType["Natural"] = "N";
    ECardholderType["Business"] = "J";
})(ECardholderType = exports.ECardholderType || (exports.ECardholderType = {}));
