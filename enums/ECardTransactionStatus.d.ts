export declare enum ECardTransactionStatus {
    Pending = "pending",
    Closed = "closed",
    Reversed = "reversed"
}
