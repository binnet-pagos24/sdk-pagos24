"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ECancelCardReasons = void 0;
var ECancelCardReasons;
(function (ECancelCardReasons) {
    ECancelCardReasons["Lost"] = "lost";
    ECancelCardReasons["Stolen"] = "stolen";
})(ECancelCardReasons = exports.ECancelCardReasons || (exports.ECancelCardReasons = {}));
