"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EDisputeReason = void 0;
var EDisputeReason;
(function (EDisputeReason) {
    EDisputeReason["Fraudulent"] = "fraudulent";
    EDisputeReason["Duplicate"] = "duplicate";
    EDisputeReason["Other"] = "other";
})(EDisputeReason = exports.EDisputeReason || (exports.EDisputeReason = {}));
