export declare enum ETimeCookie {
    Day = 24,
    HoursMinutes = 60,
    Milliseconds = 1000,
    ExpireDays = 365
}
