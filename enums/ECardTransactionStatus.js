"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ECardTransactionStatus = void 0;
var ECardTransactionStatus;
(function (ECardTransactionStatus) {
    ECardTransactionStatus["Pending"] = "pending";
    ECardTransactionStatus["Closed"] = "closed";
    ECardTransactionStatus["Reversed"] = "reversed";
})(ECardTransactionStatus = exports.ECardTransactionStatus || (exports.ECardTransactionStatus = {}));
