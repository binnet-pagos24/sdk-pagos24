"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EDisputeProductType = void 0;
var EDisputeProductType;
(function (EDisputeProductType) {
    EDisputeProductType["Service"] = "service";
    EDisputeProductType["Merchandise"] = "merchandise";
})(EDisputeProductType = exports.EDisputeProductType || (exports.EDisputeProductType = {}));
