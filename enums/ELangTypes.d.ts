export declare enum ELangTypes {
    EsVe = "es-VE",
    EnUs = "en-US",
    PtBr = "pt-BR"
}
