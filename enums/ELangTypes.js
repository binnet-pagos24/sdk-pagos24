"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ELangTypes = void 0;
var ELangTypes;
(function (ELangTypes) {
    ELangTypes["EsVe"] = "es-VE";
    ELangTypes["EnUs"] = "en-US";
    ELangTypes["PtBr"] = "pt-BR";
})(ELangTypes = exports.ELangTypes || (exports.ELangTypes = {}));
