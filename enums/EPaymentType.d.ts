export declare enum EpaymentType {
    BinCash = "BINCASH",
    Points = "POINTS",
    CreditCard = "CREDIT_CARD"
}
