"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ETimeCookie = void 0;
var ETimeCookie;
(function (ETimeCookie) {
    ETimeCookie[ETimeCookie["Day"] = 24] = "Day";
    ETimeCookie[ETimeCookie["HoursMinutes"] = 60] = "HoursMinutes";
    ETimeCookie[ETimeCookie["Milliseconds"] = 1000] = "Milliseconds";
    ETimeCookie[ETimeCookie["ExpireDays"] = 365] = "ExpireDays";
})(ETimeCookie = exports.ETimeCookie || (exports.ETimeCookie = {}));
