"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ECardType = void 0;
var ECardType;
(function (ECardType) {
    ECardType["Virtual"] = "virtual";
    ECardType["Physical"] = "physical";
})(ECardType = exports.ECardType || (exports.ECardType = {}));
