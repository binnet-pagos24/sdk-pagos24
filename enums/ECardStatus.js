"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ECardStatus = void 0;
var ECardStatus;
(function (ECardStatus) {
    ECardStatus["Active"] = "active";
    ECardStatus["Inactive"] = "inactive";
    ECardStatus["Canceled"] = "canceled";
})(ECardStatus = exports.ECardStatus || (exports.ECardStatus = {}));
