"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EpaymentType = void 0;
var EpaymentType;
(function (EpaymentType) {
    EpaymentType["BinCash"] = "BINCASH";
    EpaymentType["Points"] = "POINTS";
    EpaymentType["CreditCard"] = "CREDIT_CARD";
})(EpaymentType = exports.EpaymentType || (exports.EpaymentType = {}));
