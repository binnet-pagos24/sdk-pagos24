export declare enum ECancelCardReasons {
    Lost = "lost",
    Stolen = "stolen"
}
