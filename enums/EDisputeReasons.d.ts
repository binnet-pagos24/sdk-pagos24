export declare enum EDisputeReason {
    Fraudulent = "fraudulent",
    Duplicate = "duplicate",
    Other = "other"
}
