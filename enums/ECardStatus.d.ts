export declare enum ECardStatus {
    Active = "active",
    Inactive = "inactive",
    Canceled = "canceled"
}
