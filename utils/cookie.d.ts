import { IUserConnect } from '../interfaces/IUserConnect';
import { IUserApp } from '../interfaces/IUserApp';
/**
 * @description Get the user's cookies
 * @return string | undefined
 */
export declare const getUserCookie: () => IUserConnect | undefined;
/**
 * @description Assign value to the cookie for the user
 * @param {IUserConnect} user
 * @param {number} expireDays
 * @return void
 */
export declare const setUserCookie: (user: IUserConnect, expireDays?: number) => void;
/**
 * @description Get the pagos24 cookies
 * @param {string} userEmail
 * @param {string} appId
 * @return string | undefined
 */
export declare const getPagos24Cookie: (userEmail: string, appId: string) => string | undefined;
/**
 * @description Assign value to authorized app cookie
 * @param {string} userEmail
 * @param {string} appId
 * @param {number} expireDays
 * @return {void} void
 */
export declare const setPagos24Cookie: (userEmail: string, appId: string, userApp: IUserApp, expireDays?: number) => void;
/**
 * @description Get the pagos24 authorization cookie
 * @param {string} appId User appId
 * @return {string | undefined}
 */
export declare const getPagos24UthorizationCookie: (appId: string) => IUserApp | undefined;
/**
 * @description Assign value to authorized app cookie
 * @param {string} userEmail
 * @param {string} appId
 * @param {number} expireDays
 * @return void
 */
export declare const setAuthorizationCookie: (appId: string, userApp: IUserApp, expireDays?: number) => void;
/**
 * @description Delete a cookie
 * @param {string} cookieName
 * @return void
 */
export declare const deleteCookie: (cookieName: string) => void;
