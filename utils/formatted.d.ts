import { IConfig } from '../interfaces/IConfig';
/**
 * @description Format the data that will be sent
 * @param {string} data JSON to send
 * @return string
 */
export declare const getPlayloadFormatted: (data: {}, secretKey: string) => string;
/**
 * @description Get user auth
 * @param {IConfig} config User config
 * @return string
 */
export declare const getAuth: (config: IConfig) => string;
