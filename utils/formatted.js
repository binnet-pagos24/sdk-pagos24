"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getAuth = exports.getPlayloadFormatted = void 0;
var crypto_js_1 = require("crypto-js");
/**
 * @description Format the data that will be sent
 * @param {string} data JSON to send
 * @return string
 */
var getPlayloadFormatted = function (data, secretKey) {
    return (btoa(JSON.stringify(data)) +
        '.' +
        crypto_js_1.HmacSHA256(btoa(JSON.stringify(data)), secretKey));
};
exports.getPlayloadFormatted = getPlayloadFormatted;
/**
 * @description Get user auth
 * @param {IConfig} config User config
 * @return string
 */
var getAuth = function (config) {
    return "Basic " + config.token;
};
exports.getAuth = getAuth;
