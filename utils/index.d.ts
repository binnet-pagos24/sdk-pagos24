export * from './formatted';
export * from './cookie';
/**
 * @description Search a url for query params
 * @param name Param Name
 * @param url Url Url where to search for query params
 * @return string
 */
export declare const getParameterByName: (name: string, url?: string) => string | null;
