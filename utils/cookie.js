"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteCookie = exports.setAuthorizationCookie = exports.getPagos24UthorizationCookie = exports.setPagos24Cookie = exports.getPagos24Cookie = exports.setUserCookie = exports.getUserCookie = void 0;
var ETimeCookie_1 = require("../enums/ETimeCookie");
var nameUserCookie_1 = require("../constants/nameUserCookie");
var pagos24Key_1 = require("../constants/pagos24Key");
/**
 * @description Get the user's cookies
 * @return string | undefined
 */
var getUserCookie = function () {
    var cookieName = nameUserCookie_1.nameUserCookie;
    var decodedCookie = decodeURIComponent(document.cookie);
    var cookieArray = decodedCookie.split(';');
    var cookieFound = cookieArray.find(function (cookie) { return cookie.indexOf(cookieName) !== -1; });
    if (!cookieFound) {
        return undefined;
    }
    var cookieValue = cookieFound.replace("r3sU-" + pagos24Key_1.pagos24Key + "=", '');
    return JSON.parse(atob(cookieValue));
};
exports.getUserCookie = getUserCookie;
/**
 * @description Assign value to the cookie for the user
 * @param {IUserConnect} user
 * @param {number} expireDays
 * @return void
 */
var setUserCookie = function (user, expireDays) {
    if (expireDays === void 0) { expireDays = ETimeCookie_1.ETimeCookie.ExpireDays; }
    user.email = user.email.toLowerCase().trim();
    var dateExpire = new Date();
    var datETimeCookieExpire = expireDays *
        ETimeCookie_1.ETimeCookie.Day *
        ETimeCookie_1.ETimeCookie.HoursMinutes *
        ETimeCookie_1.ETimeCookie.HoursMinutes *
        ETimeCookie_1.ETimeCookie.Milliseconds;
    dateExpire.setTime(dateExpire.getTime() + datETimeCookieExpire);
    var expires = "expires=" + dateExpire.toUTCString();
    var cookieName = nameUserCookie_1.nameUserCookie;
    var cookieValue = btoa(JSON.stringify(user));
    document.cookie = cookieName + "=" + cookieValue + ";" + expires + ";path=/";
};
exports.setUserCookie = setUserCookie;
/**
 * @description Get the pagos24 cookies
 * @param {string} userEmail
 * @param {string} appId
 * @return string | undefined
 */
var getPagos24Cookie = function (userEmail, appId) {
    userEmail = userEmail.toLowerCase().trim();
    var cookieName = appId + "-" + userEmail + "-" + pagos24Key_1.pagos24Key;
    var decodedCookie = decodeURIComponent(document.cookie);
    var cookieArray = decodedCookie.split(';');
    var cookieFound = cookieArray.find(function (cookie) { return cookie.indexOf(cookieName) !== -1; });
    if (!cookieFound) {
        return undefined;
    }
    var cookieValue = cookieFound.replace(appId + "-" + userEmail + "-" + pagos24Key_1.pagos24Key + "=", '');
    return JSON.parse(atob(cookieValue));
};
exports.getPagos24Cookie = getPagos24Cookie;
/**
 * @description Assign value to authorized app cookie
 * @param {string} userEmail
 * @param {string} appId
 * @param {number} expireDays
 * @return {void} void
 */
var setPagos24Cookie = function (userEmail, appId, userApp, expireDays) {
    if (expireDays === void 0) { expireDays = ETimeCookie_1.ETimeCookie.ExpireDays; }
    userEmail = userEmail.toLowerCase().trim();
    var dateExpire = new Date();
    var datETimeCookieExpire = expireDays *
        ETimeCookie_1.ETimeCookie.Day *
        ETimeCookie_1.ETimeCookie.HoursMinutes *
        ETimeCookie_1.ETimeCookie.HoursMinutes *
        ETimeCookie_1.ETimeCookie.Milliseconds;
    dateExpire.setTime(dateExpire.getTime() + datETimeCookieExpire);
    var expires = "expires=" + dateExpire.toUTCString();
    var cookieName = appId + "-" + userEmail + "-" + pagos24Key_1.pagos24Key;
    var cookieValue = btoa(JSON.stringify(userApp));
    document.cookie = cookieName + "=" + cookieValue + ";" + expires + ";path=/";
};
exports.setPagos24Cookie = setPagos24Cookie;
/**
 * @description Get the pagos24 authorization cookie
 * @param {string} appId User appId
 * @return {string | undefined}
 */
var getPagos24UthorizationCookie = function (appId) {
    var cookieName = appId + "-" + pagos24Key_1.pagos24Key;
    var decodedCookie = decodeURIComponent(document.cookie);
    var cookieArray = decodedCookie.split(';');
    var cookieFound = cookieArray.find(function (cookie) { return cookie.indexOf(cookieName) !== -1; });
    if (!cookieFound) {
        return undefined;
    }
    var cookieValue = cookieFound.replace(appId + "-" + pagos24Key_1.pagos24Key + "=", '');
    return JSON.parse(atob(cookieValue));
};
exports.getPagos24UthorizationCookie = getPagos24UthorizationCookie;
/**
 * @description Assign value to authorized app cookie
 * @param {string} userEmail
 * @param {string} appId
 * @param {number} expireDays
 * @return void
 */
var setAuthorizationCookie = function (appId, userApp, expireDays) {
    if (expireDays === void 0) { expireDays = ETimeCookie_1.ETimeCookie.ExpireDays; }
    var dateExpire = new Date();
    var datETimeCookieExpire = expireDays *
        ETimeCookie_1.ETimeCookie.Day *
        ETimeCookie_1.ETimeCookie.HoursMinutes *
        ETimeCookie_1.ETimeCookie.HoursMinutes *
        ETimeCookie_1.ETimeCookie.Milliseconds;
    dateExpire.setTime(dateExpire.getTime() + datETimeCookieExpire);
    var expires = "expires=" + dateExpire.toUTCString();
    var cookieName = appId + "-" + pagos24Key_1.pagos24Key;
    var cookieValue = btoa(JSON.stringify(userApp));
    document.cookie = cookieName + "=" + cookieValue + ";" + expires + ";path=/";
};
exports.setAuthorizationCookie = setAuthorizationCookie;
/**
 * @description Delete a cookie
 * @param {string} cookieName
 * @return void
 */
var deleteCookie = function (cookieName) {
    document.cookie =
        cookieName + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
};
exports.deleteCookie = deleteCookie;
