import { LanguageType } from '../types/LanguageType';
export interface IConfig {
    token: string;
    email: string;
    secretKey: string;
    appId: string;
    projectUrl: string;
    sandbox: boolean;
    lang: LanguageType;
}
