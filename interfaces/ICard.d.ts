import { ECardStatus } from '../enums/ECardStatus';
import { ECardType } from '../enums/ICardType';
export interface ICard {
    brand: string;
    cancellation_reason: string | null;
    cardholder: {
        company: string | null;
        email: string;
        id: string;
        individual: string | null;
        metadata: Object;
        name: string | null;
        status: ECardStatus;
        type: ECardType;
    };
    id: string;
    created: string;
    currency: string;
    exp_month: number;
    exp_year: number;
    last4: string;
    shipping: {
        carrier: string;
        eta: string;
        status: string;
        tracking_number: string | null;
        tracking_url: string | null;
    } | null;
    spending_controls: object | null;
    status: ECardStatus;
    type: ECardType;
    is_expired: false;
    cardIdP24: 123;
}
