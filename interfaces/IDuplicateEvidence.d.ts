export interface IDuplicateEvidence {
    additional_documentation: string;
    explanation: string;
    original_transaction: string;
}
