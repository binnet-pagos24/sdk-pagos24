import { ICardTransaction } from './ICardTransaction';
export interface ICardTransactions {
    availableBalance: string;
    cardCurrency: string;
    transactions: ICardTransaction[];
}
