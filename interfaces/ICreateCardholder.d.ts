import { ECardholderType } from '../enums/ECardholderType';
export interface ICreateCardholder {
    p24Id: string;
    email: string;
    cardHolderId: string;
    created: number;
    name: string;
    cardholderType: ECardholderType;
}
