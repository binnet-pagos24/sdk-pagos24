import { ECardTransactionStatus } from '../enums/ECardTransactionStatus';
export interface ICardTransaction {
    amount: string;
    card: string;
    cardholder: string;
    created: string;
    currency: string;
    dispute: null;
    id: string;
    merchant_amount: number;
    merchant_currency: string;
    merchant_data: {
        category: string;
        city: string;
        country: string;
        name: string;
        network_id: string;
        postal_code: string;
        state: string;
    };
    type: 'capture';
    approved: boolean;
    status: ECardTransactionStatus;
}
