export interface IRefundPaymentIntent {
    txt_msg: string;
    title_msg: string;
    transaccion: string;
    charge: string;
    amount: number;
    currency: string;
    error: number;
}
