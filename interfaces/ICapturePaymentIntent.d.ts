export interface ICapturePaymentIntent {
    txt_msg: string;
    title: string;
    trasnsaction_id: string;
    payment_intent_id: string;
    amount: number;
    voucher: string;
    error: number;
}
