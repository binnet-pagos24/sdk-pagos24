export interface ICountry {
    countryIso: string;
    shortCountryIso: string;
    name: string;
    personalTextId: string;
    comercialTextId: string;
    cellPhoneMask: string;
    postalCodeMask: string;
    landlineMask: string;
    areaCode: string;
}
