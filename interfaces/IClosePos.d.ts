export interface IClosePos {
    transacccion: string;
    n_transac: string;
    id_tarjeta: string;
    monto: string;
    estado: string;
    descripcion: string;
    fecha_frm: string;
    fecha: string;
    operador: '+' | '-';
}
