export interface IPaymentIntent {
    id: string;
    object: string;
    amount: number;
    amount_capturable: number;
    amount_received: number;
    application: null | any;
    application_fee_amount: null | number;
    canceled_at: null | number;
    cancellation_reason: null | string;
    capture_method: string;
    charges: any;
    client_secret: string;
    confirmation_method: string;
    created: number;
    currency: string;
    customer: null | any;
    description: null | string;
    invoice: null | string;
    last_payment_error: null | any;
    livemode: boolean;
    metadata: any;
    next_action: null | any;
    on_behalf_of: null | string;
    payment_method: null | string;
    payment_method_options: any;
    payment_method_types: string[];
    receipt_email: null | string;
    review: null | string;
    setup_future_usage: null | 'on_session' | 'off_session';
    shipping: null | any;
    statement_descriptor: null | string;
    statement_descriptor_suffix: null | string;
    status: string;
    transfer_data: null | any;
    transfer_group: null | any;
}
