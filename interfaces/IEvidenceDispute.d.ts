import { EDisputeReason } from '../enums/EDisputeReasons';
import { IDuplicateEvidence } from './IDuplicateEvidence';
import { IFraudulentEvidence } from './IFraudulentEvidence';
import { IOtherEvidence } from './IOtherEvidence';
export interface IEvidenceDispute {
    other?: IOtherEvidence;
    duplicate?: IDuplicateEvidence;
    fraudulent?: IFraudulentEvidence;
    reason: EDisputeReason;
}
