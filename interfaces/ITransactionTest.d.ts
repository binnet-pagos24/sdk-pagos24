export interface ITransactionTest {
    amount: string;
    created: string;
    currency: string;
}
