export interface IFraudulentEvidence {
    additional_documentation: string;
    explanation: string;
}
