import { EDisputeProductType } from '../enums/EDisputeProductType';
export interface IOtherEvidence {
    additional_documentation: string;
    explanation: string;
    product_description: string;
    product_type: EDisputeProductType;
}
