export interface IUserApp {
    avatar: string;
    currency: string;
    error: number;
    nb_usuario: string;
    qr: string;
    email: string;
}
