import { ECardType } from '../../enums/ICardType';
export interface IRechargeCardParams {
    amount: number;
    cardIdP24: number;
    position: {
        latitude: string;
        longitude: string;
    };
    expMonth: number;
    expYear: number;
    cvc: string;
    lastFourDigitsCard: string;
    cardId: string;
    type: ECardType;
}
