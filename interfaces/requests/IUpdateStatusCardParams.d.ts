import { ECardStatus } from '../../enums/ECardStatus';
import { ECardType } from '../../enums/ICardType';
export interface IUpdateStatusCardParams {
    cvc: string;
    expMonth: number;
    expYear: number;
    lastFourDigitsCard: string;
    type: ECardType;
    status: ECardStatus;
    cardId: string;
}
