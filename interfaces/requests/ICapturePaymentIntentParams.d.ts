import { IGeolocation } from '../IGeoLocation';
export interface ICapturePaymentIntentParams {
    paymentIntentId: string;
    posId: string;
    position: IGeolocation;
}
