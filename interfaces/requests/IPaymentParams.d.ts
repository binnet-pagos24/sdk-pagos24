import { LanguageType } from '../../types/LanguageType';
import { PaymentType } from '../../types/PaymentType';
import { IGeolocation } from '../IGeoLocation';
export interface IPaymentParams {
    payerEmail: string;
    amount: number;
    OTPago: string;
    language: LanguageType;
    paymentType: PaymentType;
    creditCardNumber?: string;
    position: IGeolocation;
}
