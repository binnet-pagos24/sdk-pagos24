import { ELangTypes } from '../../enums/ELangTypes';
export interface IStateParams {
    language: string;
    countryIso: ELangTypes;
}
