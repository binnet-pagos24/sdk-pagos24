import { ECardType } from '../../enums/ICardType';
export interface IGetCardParams {
    cardId: string;
    cardholderId: string;
    type: ECardType;
}
