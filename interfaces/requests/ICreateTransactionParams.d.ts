export interface ICreateTransactionParams {
    cvc: string;
    expMonth: number;
    expYear: number;
    number: string;
}
