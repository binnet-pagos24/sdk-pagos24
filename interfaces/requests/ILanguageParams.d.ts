import { ELangTypes } from '../../enums/ELangTypes';
export interface ILanguageParams {
    language: ELangTypes;
}
