export interface IGetMyPosParam {
    debug?: boolean;
    simulated: boolean;
    idLocation: string;
}
