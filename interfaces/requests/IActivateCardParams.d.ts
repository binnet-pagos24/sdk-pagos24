export interface IActivateCardParams {
    cvc: string;
    expMonth: number;
    expYear: number;
    lastFourDigitsCard: string;
    cardIdP24: number;
    number: string;
    cardId: string;
}
