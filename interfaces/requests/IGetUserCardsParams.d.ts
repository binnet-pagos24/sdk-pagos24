import { LanguageType } from '../../types/LanguageType';
export interface IGetUserCardsParams {
    userId: string;
    language: LanguageType;
}
