import { ECancelCardReasons } from '../../enums/ECancelCardReasons';
export interface ICancelCardParams {
    cardId: string;
    reason: ECancelCardReasons;
    cardIdP24: number;
}
