import { IEvidenceDispute } from '../IEvidenceDispute';
export interface IDisputeTransactionRequest {
    transactionId: string;
    evidence: IEvidenceDispute;
}
