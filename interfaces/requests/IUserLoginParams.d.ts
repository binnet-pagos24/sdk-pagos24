import { LanguageType } from '../../types/LanguageType';
import { LoginModeType } from '../../types/LoginModeType';
export interface IUserLoginParams {
    loginMode: LoginModeType;
    language: LanguageType;
    redirectUri?: string;
}
