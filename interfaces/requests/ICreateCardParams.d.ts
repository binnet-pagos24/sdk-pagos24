import { ECardType } from '../../enums/ICardType';
export interface ICreateCardParams {
    cardholderId: string;
    type: ECardType;
    p24Id: string;
    position: {
        latitude: string;
        longitude: string;
    };
}
