import { ECardholderType } from '../../enums/ECardholderType';
export interface ICardholderParams {
    cardholderType: ECardholderType;
    email: string;
    name: string;
    lastNames?: string;
    idNumber: string;
    country: string;
    state: string;
    postalCode: string;
    city: string;
    address: string;
    phone: string;
    latitude: string;
    longitude: string;
    invitedBy: string;
    sector?: string;
    cardholderName?: string;
}
