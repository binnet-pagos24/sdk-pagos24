import { LanguageType } from '../types/LanguageType';
export interface IUserConnect {
    avatar: string;
    email: string;
    error: number;
    fullname: string;
    lang: LanguageType;
    qr: string;
    user_id: string;
}
