import { ICreditCard } from '../interfaces/ICreditCard';
import { IPayment } from '../interfaces/IPayment';
import { IUserConnect } from '../interfaces/IUserConnect';
import { IGetUserCardsParams } from '../interfaces/requests/IGetUserCardsParams';
import { IPaymentParams } from '../interfaces/requests/IPaymentParams';
import { IUserLoginParams } from '../interfaces/requests/IUserLoginParams';
import { Config } from './Config';
export declare class Payment extends Config {
    userLogged: IUserConnect | undefined;
    /**
     * @description Asigna valor a la propiedad de userLogged
     * @param {IUserConnect} userConnect user to be assigned
     * @return {void}
     */
    setUserlogged(userConnect: IUserConnect): void;
    /**
     * @description Get the uri of the QR code
     * @param {string} qrCode
     * @return {Promise<string>} Promise<string>
     */
    private getQRCodeUri;
    /**
     * @description Get the QR code to pay
     * @param {number} payAmount What is going to be paid
     * @return {Promise<string>}
     */
    generateQRCode(payAmount: number): Promise<string>;
    /**
     * @description Pay using the services of Pagos24
     * @param {PaymentType} paymentType
     * @param {IPaymentParams} paymentParams Interface to generate a payment with Pagos24
     * @param {number} creditCardNumber
     * @return {Promsie<IPayment>}
     */
    createPayment(paymentParams: IPaymentParams): Promise<IPayment>;
    /**
     * @description Pay using the points of Pagos24
     * @param {string} paymentParameters Formatted payment parameters
     * @param {string} appId API id located in the config
     * @return {Promise<IPayment>}
     */
    private payWithPoints;
    /**
     * @description Pay using the balance of Pagos24
     * @param {string} paymentParameters Formatted payment parameters
     * @param {string} appId API id located in the config
     * @return {Promise<IPayment>}
     */
    private paymentWithBinCash;
    /**
     * @description Pay with your credit card
     * @param {IPaymentParams} paymentParams
     * @return {Promise<IPayment>}
     */
    private paymentWithCreditCard;
    /**
     * @description Pay using the balance of the Pagos24 platform (BinCash or Points)
     * @param {IPaymentParams} paymentParams
     * @param {EpaymentType} type The type of transaction to carry out
     * @return {Promise<IPayment>}
     */
    private payWithPagos24Cash;
    /**
     * @description You obtain a user's card data.
     * @param {IGetUserCardsParams} userCardsParams Interface used to know which user the cards belong to
     * @return {Promise<ICreditCard[]>}
     */
    getUserCards(userCardsParams: IGetUserCardsParams): Promise<ICreditCard[]>;
    /**
     * @description Log out curren user logged
     * @return void
     */
    logoutUser(): void;
    /**
     * @description Login to a Pagos24 account
     * @param {IUserLoginParams} loginParams Interface to log in a user
     * @return {Promise<IUserConnect>}
     */
    loginUser(loginParams: IUserLoginParams): Promise<IUserConnect>;
    /**
     * @description Redirect to the login page
     * @param {LanguageType} language
     * @param {string | undefined} redirectUrl
     * @return Promise<void>
     */
    private redirectToLoginPage;
    /**
     * @description Open the login interface in a new window
     * @param {LanguageType} language
     * @return {Promise<IUserConnect>}
     */
    private openWindowLogin;
}
