"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Card = void 0;
var axios_1 = require("../config/axios");
var utils_1 = require("../utils");
var Config_1 = require("./Config");
var Card = /** @class */ (function (_super) {
    __extends(Card, _super);
    function Card() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * @description Create cardholder
     * @param {ICardholderParams} cardholderParams Object that has the necessary information to register a cardholder.
     * @returns {Promise<ICreateCardholder>} cardholder object information
     */
    Card.prototype.cardholder = function (cardholderParams) {
        return __awaiter(this, void 0, void 0, function () {
            var cardholderRequest, createResponse, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        cardholderRequest = utils_1.getPlayloadFormatted(__assign(__assign({}, cardholderParams), { sandbox: this.userConfig.sandbox }), this.userConfig.secretKey);
                        return [4 /*yield*/, axios_1.axiosPos.post("/../v1.1/card/cardholder/" + this.userConfig.appId, cardholderRequest, {
                                headers: {
                                    authorization: utils_1.getAuth(this.userConfig),
                                },
                            })];
                    case 1:
                        createResponse = (_a.sent()).data;
                        return [2 /*return*/, createResponse];
                    case 2:
                        error_1 = _a.sent();
                        return [2 /*return*/, error_1];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * @description Create Pagos24 card
     * @param {ICreateCardParams} createCardParams Object for card registration
     * @returns {Promise<IResponse<ICard>>} Created card
     */
    Card.prototype.create = function (createCardParams) {
        return __awaiter(this, void 0, void 0, function () {
            var createRequest, createResponse, error_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        createRequest = utils_1.getPlayloadFormatted(__assign(__assign({}, createCardParams), { sandbox: this.userConfig.sandbox }), this.userConfig.secretKey);
                        return [4 /*yield*/, axios_1.axiosPos.post("/../v1.1/card/create/" + this.userConfig.appId, createRequest, {
                                headers: {
                                    authorization: utils_1.getAuth(this.userConfig),
                                },
                            })];
                    case 1:
                        createResponse = (_a.sent()).data;
                        return [2 /*return*/, createResponse];
                    case 2:
                        error_2 = _a.sent();
                        return [2 /*return*/, error_2];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * @description Create Pagos24 card
     * @param {IGetCardParams} cardParams Card information
     * @returns {Promise<IResponse<ICard>>} Card object
     */
    Card.prototype.get = function (cardParams) {
        return __awaiter(this, void 0, void 0, function () {
            var getRequest, createResponse, error_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        getRequest = utils_1.getPlayloadFormatted(__assign(__assign({}, cardParams), { sandbox: this.userConfig.sandbox }), this.userConfig.secretKey);
                        return [4 /*yield*/, axios_1.axiosPos.get("/../v1.1/card/retrive/" + this.userConfig.appId + "/" + getRequest, {
                                headers: {
                                    authorization: utils_1.getAuth(this.userConfig),
                                },
                            })];
                    case 1:
                        createResponse = (_a.sent()).data;
                        return [2 /*return*/, createResponse];
                    case 2:
                        error_3 = _a.sent();
                        return [2 /*return*/, error_3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * @description Activate physical card
     * @param {IActivateCardParams} cardParams Card information
     * @returns {Promise<IResponse<ICard>>} Card object
     */
    Card.prototype.activate = function (cardParams) {
        return __awaiter(this, void 0, void 0, function () {
            var cardRequest, createResponse, error_4;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        cardRequest = utils_1.getPlayloadFormatted(__assign(__assign({}, cardParams), { sandbox: this.userConfig.sandbox }), this.userConfig.secretKey);
                        return [4 /*yield*/, axios_1.axiosPos.post("/../v1.1/card/activate/" + this.userConfig.appId, cardRequest, {
                                headers: {
                                    authorization: utils_1.getAuth(this.userConfig),
                                },
                            })];
                    case 1:
                        createResponse = (_a.sent()).data;
                        return [2 /*return*/, createResponse];
                    case 2:
                        error_4 = _a.sent();
                        return [2 /*return*/, error_4];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * @description Update card status
     * @param {IUpdateStatusCardParams} updateParams Card information
     * @returns {Promise<IResponse<ICard>>} Card object
     */
    Card.prototype.status = function (updateParams) {
        return __awaiter(this, void 0, void 0, function () {
            var cardRequest, createResponse, error_5;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        cardRequest = utils_1.getPlayloadFormatted(__assign(__assign({}, updateParams), { sandbox: this.userConfig.sandbox }), this.userConfig.secretKey);
                        return [4 /*yield*/, axios_1.axiosPos.put("/../v1.1/card/status/" + this.userConfig.appId, cardRequest, {
                                headers: {
                                    authorization: utils_1.getAuth(this.userConfig),
                                },
                            })];
                    case 1:
                        createResponse = (_a.sent()).data;
                        return [2 /*return*/, createResponse];
                    case 2:
                        error_5 = _a.sent();
                        return [2 /*return*/, error_5];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * @description Cancel card
     * @param {ICancelCardParams} cancelCardParams Card information
     * @returns {Promise<IResponse<ICard>>} Card object
     */
    Card.prototype.cancel = function (cancelCardParams) {
        return __awaiter(this, void 0, void 0, function () {
            var cardRequest, createResponse, error_6;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        cardRequest = utils_1.getPlayloadFormatted(__assign(__assign({}, cancelCardParams), { sandbox: this.userConfig.sandbox }), this.userConfig.secretKey);
                        return [4 /*yield*/, axios_1.axiosPos.delete("/../v1.1/card/cancel/" + this.userConfig.appId, {
                                headers: {
                                    authorization: utils_1.getAuth(this.userConfig),
                                },
                                data: cardRequest,
                            })];
                    case 1:
                        createResponse = (_a.sent()).data;
                        return [2 /*return*/, createResponse];
                    case 2:
                        error_6 = _a.sent();
                        return [2 /*return*/, error_6];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * @description Recharge card
     * @param {IRechargeCardParams} rechargeParams Card information
     * @returns {Promise<IResponse<IRechargeCard>>} Transaction information
     */
    Card.prototype.recharge = function (rechargeParams) {
        return __awaiter(this, void 0, void 0, function () {
            var rechargeRequest, createResponse, error_7;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        rechargeRequest = utils_1.getPlayloadFormatted(__assign(__assign({}, rechargeParams), { sandbox: this.userConfig.sandbox }), this.userConfig.secretKey);
                        return [4 /*yield*/, axios_1.axiosPos.post("/../v1.1/card/recharge/" + this.userConfig.appId, rechargeRequest, {
                                headers: {
                                    authorization: utils_1.getAuth(this.userConfig),
                                },
                            })];
                    case 1:
                        createResponse = (_a.sent()).data;
                        return [2 /*return*/, createResponse];
                    case 2:
                        error_7 = _a.sent();
                        return [2 /*return*/, error_7];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * @description View card transactions
     * @param {string} cardId Card id
     * @returns {Promise<IResponse<ICardTransactions>>} Transaction information
     */
    Card.prototype.transactions = function (cardId) {
        return __awaiter(this, void 0, void 0, function () {
            var cardRequest, transactions, error_8;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        cardRequest = utils_1.getPlayloadFormatted({
                            cardId: cardId,
                            sandbox: this.userConfig.sandbox,
                        }, this.userConfig.secretKey);
                        return [4 /*yield*/, axios_1.axiosPos.get("/../v1.1/card/transaction/list/" + this.userConfig.appId + "/" + cardRequest, {
                                headers: {
                                    authorization: utils_1.getAuth(this.userConfig),
                                },
                            })];
                    case 1:
                        transactions = (_a.sent()).data;
                        return [2 /*return*/, transactions];
                    case 2:
                        error_8 = _a.sent();
                        return [2 /*return*/, error_8];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * @description Dispute transaction
     * @param {IDisputeTransactionRequest} disputeInformation Dispute information
     * @returns {Promise<IResponse<IDisputeTransactionRequest>>} Transaction information
     */
    Card.prototype.disputeTransaction = function (disputeInformation) {
        return __awaiter(this, void 0, void 0, function () {
            var cardRequest, transactions, error_9;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        cardRequest = utils_1.getPlayloadFormatted(__assign(__assign({}, disputeInformation), { sandbox: this.userConfig.sandbox }), this.userConfig.secretKey);
                        return [4 /*yield*/, axios_1.axiosPos.post("/../v1.1/card/transaction/dispute/" + this.userConfig.appId, cardRequest, {
                                headers: {
                                    authorization: utils_1.getAuth(this.userConfig),
                                },
                            })];
                    case 1:
                        transactions = (_a.sent()).data;
                        return [2 /*return*/, transactions];
                    case 2:
                        error_9 = _a.sent();
                        return [2 /*return*/, error_9];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * @description Create test transaction
     * @param {ICreateTransactionParams} cardInformation card information
     * @returns {Promise<IResponse<IDisputeTransactionRequest>>} Transaction information
     */
    Card.prototype.createTransaction = function (cardInformation) {
        return __awaiter(this, void 0, void 0, function () {
            var cardRequest, transaction, error_10;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        cardRequest = utils_1.getPlayloadFormatted(cardInformation, this.userConfig.secretKey);
                        return [4 /*yield*/, axios_1.axiosPos.post("/../v1.1/card/transaction/test/" + this.userConfig.appId, cardRequest, {
                                headers: {
                                    authorization: utils_1.getAuth(this.userConfig),
                                },
                            })];
                    case 1:
                        transaction = (_a.sent()).data;
                        return [2 /*return*/, transaction];
                    case 2:
                        error_10 = _a.sent();
                        return [2 /*return*/, error_10];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    return Card;
}(Config_1.Config));
exports.Card = Card;
