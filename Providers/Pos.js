"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Pos = void 0;
var terminal_js_1 = require("@stripe/terminal-js");
var config_1 = require("../config");
var utils_1 = require("../utils");
var Config_1 = require("./Config");
var noTerminalResponse_1 = require("../constants/noTerminalResponse");
var axios_1 = require("../config/axios");
var Throw_1 = require("./Throw");
var Pos = /** @class */ (function (_super) {
    __extends(Pos, _super);
    function Pos() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * @description Disconnect to POS
     * @return {Promise<void>}
     */
    Pos.prototype.disconnect = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            try {
                if (!_this.terminal) {
                    return reject(noTerminalResponse_1.noTerminalResponse);
                }
                _this.terminal.disconnectReader();
            }
            catch (error) {
                return reject(error);
            }
        });
    };
    /**
     * @description Gets a payment intent by ID
     * @param {string} paymentIntentId
     * @return {Promise<IPaymentIntent>}
     */
    Pos.prototype.getPaymentIntentById = function (paymentIntentId) {
        var _this = this;
        return new Promise(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
            var paymentIntentFormatted, paymentIntentResponse, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        paymentIntentFormatted = utils_1.getPlayloadFormatted({
                            paymentIntentId: paymentIntentId,
                            sandbox: this.userConfig.sandbox,
                        }, this.userConfig.secretKey);
                        return [4 /*yield*/, axios_1.axiosPos.post("/pos/payment-intent/retrive/" + this.userConfig.appId, paymentIntentFormatted, {
                                headers: {
                                    authorization: utils_1.getAuth(this.userConfig),
                                },
                            })];
                    case 1:
                        paymentIntentResponse = (_a.sent()).data;
                        if (paymentIntentResponse.errors) {
                            return [2 /*return*/, reject(paymentIntentResponse)];
                        }
                        resolve(paymentIntentResponse.data);
                        return [3 /*break*/, 3];
                    case 2:
                        error_1 = _a.sent();
                        reject(error_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        }); });
    };
    /**
     * @description Gets the POS close POS of a specific POS
     * @param {string} posId ID of the POS from which the data is to be obtained
     * @return {Promise<IClosePos[]>}
     */
    Pos.prototype.closing = function (posId) {
        var _this = this;
        return new Promise(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
            var closePosRequest, closeResponse, error_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        closePosRequest = utils_1.getPlayloadFormatted({ posId: posId }, this.userConfig.secretKey);
                        return [4 /*yield*/, axios_1.axiosPos.post("/pos/close-pos/" + this.userConfig.appId, closePosRequest, {
                                headers: {
                                    authorization: utils_1.getAuth(this.userConfig),
                                },
                            })];
                    case 1:
                        closeResponse = (_a.sent()).data;
                        if (closeResponse.errors) {
                            return [2 /*return*/, reject(closeResponse)];
                        }
                        resolve(closeResponse.data);
                        return [3 /*break*/, 3];
                    case 2:
                        error_2 = _a.sent();
                        reject(error_2);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        }); });
    };
    /**
     * @description Performs a refund of a POS transaction
     * @param {string} paymentIntentId ID of the payment intent to be reimbursed
     * @return {Promise<IRefundPaymentIntent>}
     */
    Pos.prototype.refund = function (paymentIntentId) {
        var _this = this;
        return new Promise(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
            var refundRequest, refundResponse, error_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        refundRequest = utils_1.getPlayloadFormatted({
                            paymentIntentId: paymentIntentId,
                            sandbox: this.userConfig.sandbox,
                        }, this.userConfig.secretKey);
                        return [4 /*yield*/, axios_1.axiosPos.post("/pos/payment-intent/refund/" + this.userConfig.appId, refundRequest, {
                                headers: {
                                    authorization: utils_1.getAuth(this.userConfig),
                                },
                            })];
                    case 1:
                        refundResponse = (_a.sent()).data;
                        if (refundResponse.errors) {
                            return [2 /*return*/, reject(refundResponse)];
                        }
                        resolve(refundResponse.data);
                        return [3 /*break*/, 3];
                    case 2:
                        error_3 = _a.sent();
                        reject(error_3);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        }); });
    };
    /**
     * @description Capture a payment intent to finish processing a payment through the POS.
     * @param {ICapturePaymentIntentParams} capturePaymentIntentParams
     * @return {Promise<ICapturePaymentIntent>}
     */
    Pos.prototype.capturePaymentIntent = function (capturePaymentIntentParams) {
        var _this = this;
        return new Promise(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
            var paymentIntentId, posId, position, capturePaymentIntentRequest, captureResponse, error_4;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        paymentIntentId = capturePaymentIntentParams.paymentIntentId, posId = capturePaymentIntentParams.posId, position = capturePaymentIntentParams.position;
                        capturePaymentIntentRequest = utils_1.getPlayloadFormatted({
                            paymentIntentId: paymentIntentId,
                            posId: posId,
                            latitude: position.latitude,
                            longitude: position.longitude,
                            lang: this.userConfig.lang,
                            sandbox: this.userConfig.sandbox,
                            url: this.userConfig.projectUrl,
                        }, this.userConfig.secretKey);
                        return [4 /*yield*/, axios_1.axiosPos.post("/pos/payment-intent/capture/" + this.userConfig.appId, capturePaymentIntentRequest, {
                                headers: {
                                    authorization: utils_1.getAuth(this.userConfig),
                                },
                            })];
                    case 1:
                        captureResponse = (_a.sent()).data;
                        if (captureResponse.errors) {
                            return [2 /*return*/, reject(captureResponse)];
                        }
                        resolve(captureResponse.data);
                        return [3 /*break*/, 3];
                    case 2:
                        error_4 = _a.sent();
                        reject(error_4);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        }); });
    };
    /**
     * @description Processes a payment through the POS terminal
     * @param {string} clientSecret Secret key given at the time of creating a payment intent
     * @return {Promise<IPaymentIntent>}
     */
    Pos.prototype.payment = function (clientSecret) {
        var _this = this;
        return new Promise(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
            var collectPaymentResponse, processPaymentResponse, error_5;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        if (!this.terminal) {
                            return [2 /*return*/, reject(noTerminalResponse_1.noTerminalResponse)];
                        }
                        return [4 /*yield*/, this.terminal.collectPaymentMethod(clientSecret)];
                    case 1:
                        collectPaymentResponse = _a.sent();
                        // @ts-ignore
                        if (collectPaymentResponse.error) {
                            // @ts-ignore
                            return [2 /*return*/, reject(collectPaymentResponse.error)];
                        }
                        return [4 /*yield*/, this.terminal.processPayment(
                            // @ts-ignore
                            collectPaymentResponse.paymentIntent)];
                    case 2:
                        processPaymentResponse = _a.sent();
                        // @ts-ignore
                        if (processPaymentResponse.error) {
                            // @ts-ignore
                            return [2 /*return*/, reject(processPaymentResponse.error)];
                        }
                        // @ts-ignore
                        resolve(processPaymentResponse.paymentIntent);
                        return [3 /*break*/, 4];
                    case 3:
                        error_5 = _a.sent();
                        reject(error_5);
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        }); });
    };
    /**
     * @description Create a payment intent to be able to pay via POS
     * @param {ICreatePaymentIntent} createPaymentIntentParams
     * @return {Promise<IPaymentIntent>}
     */
    Pos.prototype.createPaymentIntent = function (createPaymentIntentParams) {
        var _this = this;
        return new Promise(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
            var amount, description, createPaymentIntentRequest, paymentResponse, paymentPosResponse, error_6;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        amount = createPaymentIntentParams.amount, description = createPaymentIntentParams.description;
                        createPaymentIntentRequest = utils_1.getPlayloadFormatted({
                            sandbox: this.userConfig.sandbox,
                            amount: amount,
                            description: description,
                        }, this.userConfig.secretKey);
                        return [4 /*yield*/, axios_1.axiosPos.post("/pos/payment-intent/" + this.userConfig.appId, createPaymentIntentRequest, {
                                headers: {
                                    authorization: utils_1.getAuth(this.userConfig),
                                },
                            })];
                    case 1:
                        paymentResponse = (_a.sent()).data;
                        if (paymentResponse.errors) {
                            return [2 /*return*/, reject(paymentResponse)];
                        }
                        return [4 /*yield*/, this.payment(paymentResponse.data.client_secret)];
                    case 2:
                        paymentPosResponse = _a.sent();
                        resolve(paymentPosResponse);
                        return [3 /*break*/, 4];
                    case 3:
                        error_6 = _a.sent();
                        reject(error_6);
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        }); });
    };
    /**
     * @description Connect to POS
     * @param {IPOS} pos FULL object of the POS to be connected to
     * @return {Promise<IPOS>}
     */
    Pos.prototype.connectTo = function (pos) {
        var _this = this;
        return new Promise(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
            var readerConnect, error_7;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        if (!this.terminal) {
                            return [2 /*return*/, reject(noTerminalResponse_1.noTerminalResponse)];
                        }
                        return [4 /*yield*/, this.terminal.connectReader(pos)];
                    case 1:
                        readerConnect = _a.sent();
                        if ('error' in readerConnect) {
                            return [2 /*return*/, reject(readerConnect.error)];
                        }
                        resolve(readerConnect.reader);
                        return [3 /*break*/, 3];
                    case 2:
                        error_7 = _a.sent();
                        reject(error_7);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        }); });
    };
    /**
     * @description Obtains the POS affiliated to the Pagos24 account.
     * @param {IGetMyPosParam} myPosParams Parameters to obtain the POS of a user
     * @return {Promise<IPOS[]>}
     */
    Pos.prototype.list = function (myPosParams) {
        var _this = this;
        return new Promise(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
            var debug, simulated, idLocation, StripeTerminal, isSilumated, readers, error_8;
            var _this = this;
            var _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        debug = myPosParams.debug, simulated = myPosParams.simulated, idLocation = myPosParams.idLocation;
                        _b.label = 1;
                    case 1:
                        _b.trys.push([1, 4, , 5]);
                        if (!window) {
                            return [2 /*return*/, reject(Throw_1.Throw.error('There is no instance of window'))];
                        }
                        return [4 /*yield*/, terminal_js_1.loadStripeTerminal()];
                    case 2:
                        StripeTerminal = _b.sent();
                        this.terminal = StripeTerminal === null || StripeTerminal === void 0 ? void 0 : StripeTerminal.create({
                            onFetchConnectionToken: function () { return __awaiter(_this, void 0, void 0, function () {
                                return __generator(this, function (_a) {
                                    return [2 /*return*/, fetch(config_1.config.roleAuthUrl + "/pos/connection/" + this.userConfig.appId, {
                                            method: 'POST',
                                            headers: {
                                                authorization: utils_1.getAuth(this.userConfig),
                                            },
                                            body: utils_1.getPlayloadFormatted({ sandbox: this.userConfig.sandbox }, this.userConfig.secretKey),
                                        })
                                            .then(function (response) { return response.json(); })
                                            .then(function (data) { return data.data.secret; })
                                            .catch(function (err) { return reject(err); })];
                                });
                            }); },
                            onConnectionStatusChange: function (status) {
                                if (debug) {
                                    console.log("POS Status: " + status.status);
                                }
                            },
                            onUnexpectedReaderDisconnect: function (event) {
                                if (debug) {
                                    console.error(event);
                                }
                            },
                        });
                        isSilumated = this.userConfig.sandbox || simulated;
                        return [4 /*yield*/, ((_a = this.terminal) === null || _a === void 0 ? void 0 : _a.discoverReaders({
                                simulated: isSilumated,
                                location: idLocation,
                            }))];
                    case 3:
                        readers = _b.sent();
                        // @ts-ignore
                        if (readers.error) {
                            reject(readers);
                        }
                        resolve(readers.discoveredReaders);
                        return [3 /*break*/, 5];
                    case 4:
                        error_8 = _b.sent();
                        reject(error_8);
                        return [3 /*break*/, 5];
                    case 5: return [2 /*return*/];
                }
            });
        }); });
    };
    return Pos;
}(Config_1.Config));
exports.Pos = Pos;
