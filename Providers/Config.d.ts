import { IConfig } from '../interfaces/IConfig';
export declare abstract class Config {
    protected userConfig: IConfig;
    constructor(config: IConfig);
}
