import { ICard } from '../interfaces/ICard';
import { ICardTransactions } from '../interfaces/ICardTransactions';
import { ICreateCardholder } from '../interfaces/ICreateCardholder';
import { IRechargeCard } from '../interfaces/IRechargeCard';
import { IResponse } from '../interfaces/IResponse';
import { ITransactionTest } from '../interfaces/ITransactionTest';
import { IActivateCardParams } from '../interfaces/requests/IActivateCardParams';
import { ICancelCardParams } from '../interfaces/requests/ICancelCardParams';
import { ICardholderParams } from '../interfaces/requests/ICardholderParams';
import { ICreateCardParams } from '../interfaces/requests/ICreateCardParams';
import { ICreateTransactionParams } from '../interfaces/requests/ICreateTransactionParams';
import { IDisputeTransactionRequest } from '../interfaces/requests/IDisputeTransactionRequest';
import { IGetCardParams } from '../interfaces/requests/IGetCardParams';
import { IRechargeCardParams } from '../interfaces/requests/IRechargeCardParams';
import { IUpdateStatusCardParams } from '../interfaces/requests/IUpdateStatusCardParams';
import { Config } from './Config';
export declare class Card extends Config {
    /**
     * @description Create cardholder
     * @param {ICardholderParams} cardholderParams Object that has the necessary information to register a cardholder.
     * @returns {Promise<ICreateCardholder>} cardholder object information
     */
    cardholder(cardholderParams: ICardholderParams): Promise<IResponse<ICreateCardholder>>;
    /**
     * @description Create Pagos24 card
     * @param {ICreateCardParams} createCardParams Object for card registration
     * @returns {Promise<IResponse<ICard>>} Created card
     */
    create(createCardParams: ICreateCardParams): Promise<IResponse<ICard>>;
    /**
     * @description Create Pagos24 card
     * @param {IGetCardParams} cardParams Card information
     * @returns {Promise<IResponse<ICard>>} Card object
     */
    get(cardParams: IGetCardParams): Promise<IResponse<ICard>>;
    /**
     * @description Activate physical card
     * @param {IActivateCardParams} cardParams Card information
     * @returns {Promise<IResponse<ICard>>} Card object
     */
    activate(cardParams: IActivateCardParams): Promise<IResponse<ICard>>;
    /**
     * @description Update card status
     * @param {IUpdateStatusCardParams} updateParams Card information
     * @returns {Promise<IResponse<ICard>>} Card object
     */
    status(updateParams: IUpdateStatusCardParams): Promise<IResponse<ICard>>;
    /**
     * @description Cancel card
     * @param {ICancelCardParams} cancelCardParams Card information
     * @returns {Promise<IResponse<ICard>>} Card object
     */
    cancel(cancelCardParams: ICancelCardParams): Promise<IResponse<ICard>>;
    /**
     * @description Recharge card
     * @param {IRechargeCardParams} rechargeParams Card information
     * @returns {Promise<IResponse<IRechargeCard>>} Transaction information
     */
    recharge(rechargeParams: IRechargeCardParams): Promise<IResponse<IRechargeCard>>;
    /**
     * @description View card transactions
     * @param {string} cardId Card id
     * @returns {Promise<IResponse<ICardTransactions>>} Transaction information
     */
    transactions(cardId: string): Promise<IResponse<ICardTransactions>>;
    /**
     * @description Dispute transaction
     * @param {IDisputeTransactionRequest} disputeInformation Dispute information
     * @returns {Promise<IResponse<IDisputeTransactionRequest>>} Transaction information
     */
    disputeTransaction(disputeInformation: IDisputeTransactionRequest): Promise<IResponse<IDisputeTransactionRequest>>;
    /**
     * @description Create test transaction
     * @param {ICreateTransactionParams} cardInformation card information
     * @returns {Promise<IResponse<IDisputeTransactionRequest>>} Transaction information
     */
    createTransaction(cardInformation: ICreateTransactionParams): Promise<IResponse<ITransactionTest>>;
}
