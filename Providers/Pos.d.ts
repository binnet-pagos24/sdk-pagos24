import { IGetMyPosParam } from '../interfaces/requests/IGetMyPosParams';
import { IPOS } from '../interfaces/IPOS';
import { Config } from './Config';
import { ICreatePaymentIntent } from '../interfaces/requests/ICreatePaymentIntent.';
import { IPaymentIntent } from '../interfaces/IPaymentIntent';
import { ICapturePaymentIntentParams } from '../interfaces/requests/ICapturePaymentIntentParams';
import { ICapturePaymentIntent } from '../interfaces/ICapturePaymentIntent';
import { IRefundPaymentIntent } from '../interfaces/IRefundPaymentIntent';
import { IClosePos } from '../interfaces/IClosePos';
export declare class Pos extends Config {
    private terminal;
    /**
     * @description Disconnect to POS
     * @return {Promise<void>}
     */
    disconnect(): Promise<void>;
    /**
     * @description Gets a payment intent by ID
     * @param {string} paymentIntentId
     * @return {Promise<IPaymentIntent>}
     */
    getPaymentIntentById(paymentIntentId: string): Promise<IPaymentIntent>;
    /**
     * @description Gets the POS close POS of a specific POS
     * @param {string} posId ID of the POS from which the data is to be obtained
     * @return {Promise<IClosePos[]>}
     */
    closing(posId: string): Promise<IClosePos[]>;
    /**
     * @description Performs a refund of a POS transaction
     * @param {string} paymentIntentId ID of the payment intent to be reimbursed
     * @return {Promise<IRefundPaymentIntent>}
     */
    refund(paymentIntentId: string): Promise<IRefundPaymentIntent>;
    /**
     * @description Capture a payment intent to finish processing a payment through the POS.
     * @param {ICapturePaymentIntentParams} capturePaymentIntentParams
     * @return {Promise<ICapturePaymentIntent>}
     */
    capturePaymentIntent(capturePaymentIntentParams: ICapturePaymentIntentParams): Promise<ICapturePaymentIntent>;
    /**
     * @description Processes a payment through the POS terminal
     * @param {string} clientSecret Secret key given at the time of creating a payment intent
     * @return {Promise<IPaymentIntent>}
     */
    private payment;
    /**
     * @description Create a payment intent to be able to pay via POS
     * @param {ICreatePaymentIntent} createPaymentIntentParams
     * @return {Promise<IPaymentIntent>}
     */
    createPaymentIntent(createPaymentIntentParams: ICreatePaymentIntent): Promise<IPaymentIntent>;
    /**
     * @description Connect to POS
     * @param {IPOS} pos FULL object of the POS to be connected to
     * @return {Promise<IPOS>}
     */
    connectTo(pos: IPOS): Promise<IPOS>;
    /**
     * @description Obtains the POS affiliated to the Pagos24 account.
     * @param {IGetMyPosParam} myPosParams Parameters to obtain the POS of a user
     * @return {Promise<IPOS[]>}
     */
    list(myPosParams: IGetMyPosParam): Promise<IPOS[]>;
}
