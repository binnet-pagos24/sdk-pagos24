"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Throw = void 0;
var Throw = /** @class */ (function () {
    function Throw() {
    }
    /**
     * @description Creates a bad request error
     * @param {string} customMessage Personalized response message
     * @return {IThrowResponse}
     */
    Throw.error = function (customMessage) {
        return {
            message: customMessage,
        };
    };
    return Throw;
}());
exports.Throw = Throw;
