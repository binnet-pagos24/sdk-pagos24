"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Config = void 0;
var Config = /** @class */ (function () {
    function Config(config) {
        this.userConfig = config;
    }
    return Config;
}());
exports.Config = Config;
