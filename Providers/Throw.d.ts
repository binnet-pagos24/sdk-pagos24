import { IThrowResponse } from '../interfaces/IThrowResponse';
export declare class Throw {
    /**
     * @description Creates a bad request error
     * @param {string} customMessage Personalized response message
     * @return {IThrowResponse}
     */
    static error(customMessage: string): IThrowResponse;
}
