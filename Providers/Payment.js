"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Payment = void 0;
var config_1 = require("../config");
var axios_1 = __importDefault(require("../config/axios"));
var pagos24Key_1 = require("../constants/pagos24Key");
var ELoginMode_1 = require("../enums/ELoginMode");
var EPaymentType_1 = require("../enums/EPaymentType");
var utils_1 = require("../utils");
var Config_1 = require("./Config");
var Throw_1 = require("./Throw");
var qrcode_1 = require("qrcode");
var Payment = /** @class */ (function (_super) {
    __extends(Payment, _super);
    function Payment() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * @description Asigna valor a la propiedad de userLogged
     * @param {IUserConnect} userConnect user to be assigned
     * @return {void}
     */
    Payment.prototype.setUserlogged = function (userConnect) {
        this.userLogged = userConnect;
    };
    /**
     * @description Get the uri of the QR code
     * @param {string} qrCode
     * @return {Promise<string>} Promise<string>
     */
    Payment.prototype.getQRCodeUri = function (qrCode) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve, reject) {
                        qrcode_1.toDataURL(qrCode, function (err, uri) {
                            if (err) {
                                reject(err);
                                return;
                            }
                            resolve(uri);
                        });
                    })];
            });
        });
    };
    /**
     * @description Get the QR code to pay
     * @param {number} payAmount What is going to be paid
     * @return {Promise<string>}
     */
    Payment.prototype.generateQRCode = function (payAmount) {
        return __awaiter(this, void 0, void 0, function () {
            var payAmountStr;
            var _this = this;
            return __generator(this, function (_a) {
                payAmountStr = payAmount.toString();
                return [2 /*return*/, new Promise(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
                        var user, qrCode, qrCodeUri, error_1;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    _a.trys.push([0, 2, , 3]);
                                    user = utils_1.getPagos24UthorizationCookie(this.userConfig.appId);
                                    if (payAmountStr.indexOf('.') !== -1 ||
                                        payAmountStr.indexOf(',') !== -1) {
                                        payAmountStr = payAmountStr.replace('.', '');
                                        payAmountStr = payAmountStr.replace(',', '');
                                    }
                                    else {
                                        payAmountStr = payAmountStr + '00';
                                    }
                                    qrCode = (user === null || user === void 0 ? void 0 : user.qr) + ":000:" + payAmountStr;
                                    return [4 /*yield*/, this.getQRCodeUri(qrCode)];
                                case 1:
                                    qrCodeUri = _a.sent();
                                    resolve(qrCodeUri);
                                    return [3 /*break*/, 3];
                                case 2:
                                    error_1 = _a.sent();
                                    reject(error_1);
                                    return [3 /*break*/, 3];
                                case 3: return [2 /*return*/];
                            }
                        });
                    }); })];
            });
        });
    };
    /**
     * @description Pay using the services of Pagos24
     * @param {PaymentType} paymentType
     * @param {IPaymentParams} paymentParams Interface to generate a payment with Pagos24
     * @param {number} creditCardNumber
     * @return {Promsie<IPayment>}
     */
    Payment.prototype.createPayment = function (paymentParams) {
        var _this = this;
        return new Promise(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
            var paymentResponse, _a, error_2;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 6, , 7]);
                        paymentResponse = void 0;
                        _a = paymentParams.paymentType;
                        switch (_a) {
                            case EPaymentType_1.EpaymentType.CreditCard: return [3 /*break*/, 1];
                        }
                        return [3 /*break*/, 3];
                    case 1: return [4 /*yield*/, this.paymentWithCreditCard(paymentParams)];
                    case 2:
                        paymentResponse = _b.sent();
                        return [3 /*break*/, 5];
                    case 3: return [4 /*yield*/, this.payWithPagos24Cash(paymentParams, paymentParams.paymentType)];
                    case 4:
                        paymentResponse = _b.sent();
                        _b.label = 5;
                    case 5:
                        resolve(paymentResponse);
                        return [3 /*break*/, 7];
                    case 6:
                        error_2 = _b.sent();
                        reject(error_2);
                        return [3 /*break*/, 7];
                    case 7: return [2 /*return*/];
                }
            });
        }); });
    };
    /**
     * @description Pay using the points of Pagos24
     * @param {string} paymentParameters Formatted payment parameters
     * @param {string} appId API id located in the config
     * @return {Promise<IPayment>}
     */
    Payment.prototype.payWithPoints = function (paymentParameters, appId) {
        var _this = this;
        return new Promise(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
            var payWithPointsResponse, error_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, axios_1.default.post("/exec/payment-points/" + appId, paymentParameters, {
                                headers: {
                                    authorization: utils_1.getAuth(this.userConfig),
                                },
                            })];
                    case 1:
                        payWithPointsResponse = (_a.sent()).data;
                        resolve(payWithPointsResponse);
                        return [3 /*break*/, 3];
                    case 2:
                        error_3 = _a.sent();
                        reject(error_3);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        }); });
    };
    /**
     * @description Pay using the balance of Pagos24
     * @param {string} paymentParameters Formatted payment parameters
     * @param {string} appId API id located in the config
     * @return {Promise<IPayment>}
     */
    Payment.prototype.paymentWithBinCash = function (paymentParameters, appId) {
        var _this = this;
        return new Promise(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
            var payWithBinCashResponse, error_4;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, axios_1.default.post("/exec/payment-bincash/" + appId, paymentParameters, {
                                headers: {
                                    authorization: utils_1.getAuth(this.userConfig),
                                },
                            })];
                    case 1:
                        payWithBinCashResponse = (_a.sent()).data;
                        resolve(payWithBinCashResponse);
                        return [3 /*break*/, 3];
                    case 2:
                        error_4 = _a.sent();
                        reject(error_4);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        }); });
    };
    /**
     * @description Pay with your credit card
     * @param {IPaymentParams} paymentParams
     * @return {Promise<IPayment>}
     */
    Payment.prototype.paymentWithCreditCard = function (paymentParams) {
        var _this = this;
        return new Promise(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
            var payerEmail, amount, OTPago, language, creditCardNumber, position, paymentPlayload, paymentFormatted, appId, paymentResponse, error_5;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        payerEmail = paymentParams.payerEmail, amount = paymentParams.amount, OTPago = paymentParams.OTPago, language = paymentParams.language, creditCardNumber = paymentParams.creditCardNumber, position = paymentParams.position;
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        paymentPlayload = {
                            payer_email: payerEmail,
                            benefic_email: this.userConfig.email,
                            amount: amount,
                            pin: OTPago,
                            url: this.userConfig.projectUrl,
                            position: {
                                latitud: position.latitude,
                                longitud: position.longitude,
                            },
                            lang: language,
                            sandbox: this.userConfig.sandbox,
                            tarjeta: creditCardNumber,
                        };
                        paymentFormatted = utils_1.getPlayloadFormatted(paymentPlayload, this.userConfig.secretKey);
                        appId = this.userConfig.appId;
                        return [4 /*yield*/, axios_1.default.post("/exec/payment-credit-card/" + appId, paymentFormatted, {
                                headers: {
                                    authorization: utils_1.getAuth(this.userConfig),
                                },
                            })];
                    case 2:
                        paymentResponse = (_a.sent()).data;
                        resolve(paymentResponse);
                        return [3 /*break*/, 4];
                    case 3:
                        error_5 = _a.sent();
                        reject(error_5);
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        }); });
    };
    /**
     * @description Pay using the balance of the Pagos24 platform (BinCash or Points)
     * @param {IPaymentParams} paymentParams
     * @param {EpaymentType} type The type of transaction to carry out
     * @return {Promise<IPayment>}
     */
    Payment.prototype.payWithPagos24Cash = function (paymentParams, type) {
        var _this = this;
        return new Promise(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
            var payerEmail, amount, OTPago, language, position, paymentPlayload, paymentFormatted, appId, paymentResponse, _a, error_6;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 6, , 7]);
                        payerEmail = paymentParams.payerEmail, amount = paymentParams.amount, OTPago = paymentParams.OTPago, language = paymentParams.language, position = paymentParams.position;
                        paymentPlayload = {
                            payer_email: payerEmail,
                            benefic_email: this.userConfig.email,
                            amount: amount,
                            pin: OTPago,
                            url: this.userConfig.projectUrl,
                            position: {
                                latitud: position.latitude,
                                longitud: position.longitude,
                            },
                            lang: language,
                            sandbox: this.userConfig.sandbox,
                        };
                        paymentFormatted = utils_1.getPlayloadFormatted(paymentPlayload, String(this.userConfig.secretKey));
                        appId = String(this.userConfig.appId);
                        paymentResponse = void 0;
                        _a = type;
                        switch (_a) {
                            case EPaymentType_1.EpaymentType.BinCash: return [3 /*break*/, 1];
                        }
                        return [3 /*break*/, 3];
                    case 1: return [4 /*yield*/, this.paymentWithBinCash(paymentFormatted, appId)];
                    case 2:
                        paymentResponse = _b.sent();
                        return [3 /*break*/, 5];
                    case 3: return [4 /*yield*/, this.payWithPoints(paymentFormatted, appId)];
                    case 4:
                        paymentResponse = _b.sent();
                        _b.label = 5;
                    case 5:
                        resolve(paymentResponse);
                        return [3 /*break*/, 7];
                    case 6:
                        error_6 = _b.sent();
                        reject(error_6);
                        return [3 /*break*/, 7];
                    case 7: return [2 /*return*/];
                }
            });
        }); });
    };
    /**
     * @description You obtain a user's card data.
     * @param {IGetUserCardsParams} userCardsParams Interface used to know which user the cards belong to
     * @return {Promise<ICreditCard[]>}
     */
    Payment.prototype.getUserCards = function (userCardsParams) {
        var _this = this;
        return new Promise(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
            var userId, language, userRequest, data, error_7;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        userId = userCardsParams.userId, language = userCardsParams.language;
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        userRequest = utils_1.getPlayloadFormatted({
                            user_id: userId,
                            sandbox: this.userConfig.sandbox,
                            lang: language,
                        }, this.userConfig.secretKey);
                        return [4 /*yield*/, axios_1.default.post("/exec/conect/" + this.userConfig.appId, userRequest, {
                                headers: {
                                    authorization: utils_1.getAuth(this.userConfig),
                                },
                            })];
                    case 2:
                        data = (_a.sent()).data;
                        return [2 /*return*/, resolve(data)];
                    case 3:
                        error_7 = _a.sent();
                        return [2 /*return*/, reject(error_7)];
                    case 4: return [2 /*return*/];
                }
            });
        }); });
    };
    /**
     * @description Log out curren user logged
     * @return void
     */
    Payment.prototype.logoutUser = function () {
        utils_1.deleteCookie(this.userConfig.appId + "-" + this.userConfig.email + "-" + pagos24Key_1.pagos24Key);
        utils_1.deleteCookie("r3sU-" + pagos24Key_1.pagos24Key);
    };
    /**
     * @description Login to a Pagos24 account
     * @param {IUserLoginParams} loginParams Interface to log in a user
     * @return {Promise<IUserConnect>}
     */
    Payment.prototype.loginUser = function (loginParams) {
        var _this = this;
        var language = loginParams.language, loginMode = loginParams.loginMode, redirectUri = loginParams.redirectUri;
        return new Promise(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
            var userConnected;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (utils_1.getUserCookie()) {
                            return [2 /*return*/, resolve(utils_1.getUserCookie())];
                        }
                        if (!(loginMode === ELoginMode_1.ELoginMode.Window)) return [3 /*break*/, 2];
                        delete loginParams.redirectUri;
                        if (!window) {
                            return [2 /*return*/, reject(Throw_1.Throw.error('There is no instance of window'))];
                        }
                        return [4 /*yield*/, this.openWindowLogin(language)];
                    case 1:
                        userConnected = _a.sent();
                        return [2 /*return*/, resolve(userConnected)];
                    case 2:
                        if (loginMode === ELoginMode_1.ELoginMode.Redirect) {
                            this.redirectToLoginPage(language, redirectUri);
                            return [2 /*return*/];
                        }
                        return [2 /*return*/];
                }
            });
        }); });
    };
    /**
     * @description Redirect to the login page
     * @param {LanguageType} language
     * @param {string | undefined} redirectUrl
     * @return Promise<void>
     */
    Payment.prototype.redirectToLoginPage = function (language, redirectUrl) {
        return __awaiter(this, void 0, void 0, function () {
            var url;
            return __generator(this, function (_a) {
                try {
                    if (!redirectUrl) {
                        return [2 /*return*/, Promise.reject({
                                message: 'No redirect url',
                                success: false,
                            })];
                    }
                    url = config_1.config.loginUri + "/auth/login?lang=" + language + "&appId=" + this.userConfig.appId + "&auth=" + utils_1.getAuth(this.userConfig)
                        .replace('Basic', '')
                        .trim() + "&redirectUri=" + redirectUrl;
                    window.location.href = url;
                }
                catch (error) {
                    return [2 /*return*/, Promise.reject(error)];
                }
                return [2 /*return*/];
            });
        });
    };
    /**
     * @description Open the login interface in a new window
     * @param {LanguageType} language
     * @return {Promise<IUserConnect>}
     */
    Payment.prototype.openWindowLogin = function (language) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve, reject) {
                        try {
                            var url = config_1.config.loginUri + "/auth/login?lang=" + language + "&appId=" + _this.userConfig.appId + "&auth=" + utils_1.getAuth(_this.userConfig).replace('Basic', '').trim();
                            window.open(url, '', 'width=500,height=500,top=0,left=800');
                            window.onmessage = function (_a) {
                                var data = _a.data;
                                utils_1.setUserCookie(data);
                                _this.setUserlogged(data);
                                resolve(data);
                            };
                        }
                        catch (error) {
                            reject(error);
                        }
                    })];
            });
        });
    };
    return Payment;
}(Config_1.Config));
exports.Payment = Payment;
