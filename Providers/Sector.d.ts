import { IResponse } from '../interfaces/IResponse';
import { ISector } from '../interfaces/ISector';
import { ILanguageParams } from '../interfaces/requests/ILanguageParams';
import { Config } from './Config';
export declare class Sector extends Config {
    /**
     * @description Get all sectors
     * @param {ELangTypes} sectorInformation
     * @return {Promise<IResponse<ISector>>}
     */
    getAll(sectorInformation: ILanguageParams): Promise<IResponse<ISector>>;
}
