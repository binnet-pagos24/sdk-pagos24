import { ICountry } from '../interfaces/ICountry';
import { IResponse } from '../interfaces/IResponse';
import { IState } from '../interfaces/IState';
import { ILanguageParams } from '../interfaces/requests/ILanguageParams';
import { IStateParams } from '../interfaces/requests/IStateParams';
import { Config } from './Config';
export declare class Country extends Config {
    /**
     * @description Get all avalible countries
     * @param {ILanguageParams} countryInformation
     * @return {Promise<IResponse<ICountry>>}
     */
    getAll(countryInformation: ILanguageParams): Promise<IResponse<ICountry>>;
    /**
     * @description Get all country states
     * @param {IStateParams} statesInformation
     * @return {Promise<IResponse<IState[]>>}
     */
    states(statesInformation: IStateParams): Promise<IResponse<IState[]>>;
}
