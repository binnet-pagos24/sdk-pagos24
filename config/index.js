"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.config = void 0;
exports.config = {
    url: 'https://api.pagos24.com/v1',
    loginUri: 'https://auth.pagos24.com',
    roleAuthUrl: 'https://payments-api.pagos24.app',
};
