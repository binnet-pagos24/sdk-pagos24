"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.axiosPos = void 0;
var axios_1 = __importDefault(require("axios"));
var _1 = require(".");
var axiosInstance = axios_1.default.create({
    baseURL: _1.config.url,
    headers: {
        'content-type': 'text/plain',
    },
});
exports.axiosPos = axios_1.default.create({
    baseURL: _1.config.roleAuthUrl,
    headers: {
        'content-type': 'text/plain',
    },
});
exports.default = axiosInstance;
