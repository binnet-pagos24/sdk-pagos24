# Pagos24 SDK

The Pagos24 SDK is a tool that you will be able to use to integrate all Pagos24
services in your web app. From making online payments to making physical
payments through a POS.

## Intallation

```bash
npm install pagos24-js
```

## Usage

```typescript
import Pagos24 from 'pagos24-js';

const pagos24 = new Pagos24({
    token: 'ZXhhbXBsZUBlbWFpbC5jb206WlRoZk1UaGhjMlE0UVRFNVlYTmtNMU00WVE9PQ==';
    email: 'example@email.com';
    secretKey: 'yourSecretKey';
    appId: 'ZVk4ZlNEOTg0c2RzM1A4MmRh';
    projectUrl: 'https://www.yourweburl.com';
    sandbox: true;
    lang: 'en-US';
});
```

| Parameters | Description                                                                                                     |
| ---------- | --------------------------------------------------------------------------------------------------------------- |
| token      | Authorization token found in Pagos24 panel                                                                      |
| email      | Your e-mail address                                                                                             |
| secretKey  | Your secret key registered with Pagos24                                                                         |
| appId      | App ID generated in the Pagos24 panel                                                                           |
| projectUrl | Reference URL of your project                                                                                   |
| sanbox     | Specifies if the library is in test or production mode (Recommended to use true if the library is being tested) |
| lang       | Response language. enum (`es-VE`, `en-US`, `pt-BR`)                                                             |

## Documentation

To learn about all the functions of the Pagos24 SDK, we invite you to see our
[documentation](https://www.pagos24.com/doc).
