"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.noTerminalResponse = void 0;
exports.noTerminalResponse = {
    count: 1,
    data: null,
    errors: 'NO_GET_POS',
    message: 'The function to obtain the POS has not been executed. (getMyPos)',
    statusCode: 400,
};
